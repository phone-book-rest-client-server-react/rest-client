import React from 'react';
import './App.css';
import PhoneBookContainer from './containers/PhoneBookContainer';

const App = () => {
  return (
    <div className="App">
      <div className="App-header">
        PhoneBook App by Kevin Ready
      </div>
      <PhoneBookContainer />
    </div>
  );
};

export default App;

