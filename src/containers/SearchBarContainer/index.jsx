import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import SearchBar from '../../components/SearchBar';
import { getPhone } from '../../services/actions';
import store from '../../services/store';

class SearchBarContainer extends Component {
  static propTypes={
    toggleMe: PropTypes.func,
    className: PropTypes.string,
  }
  constructor(props) {
    super(props);
    this.state = { phoneitems: [], phoneitem: null };
    store.subscribe(() => {
      this.setState({
        phoneitems: store.getState().initPhoneBook.phonebook,
      });
    });
  }
  
  componentWillUpdate(nextProps, nextState) {
    return (
      (this.props.phoneitems !== nextProps) ||
      (this.props.phoneitem !== nextProps)
      );
  }
  
  render() {
    if (store.getState().toggle.payload && store.getState().toggle.payload.nav){
      return <SearchBar className={store.getState().toggle.payload.nav} toggleMe={this.props.toggleMe} phoneitems={this.state.phoneitems} />;
    }
    return <SearchBar toggleMe={this.props.toggleMe} phoneitems={this.state.phoneitems} />;
  }
}

function mapStateToProps(state){
  return {
    phoneitems: state.phoneitems,
  }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ updatePhoneBook: getPhone }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchBarContainer);


