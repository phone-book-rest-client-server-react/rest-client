import React from 'react';
import { Provider } from 'react-redux';
import chai, { expect, assert } from 'chai';
import chaiEnzyme from 'chai-enzyme';
import { mount } from 'enzyme';
import SearchBarContainer from './';
import store from '../../services/store';

chai.use(chaiEnzyme());

const wrapper = mount(<Provider store={store}><SearchBarContainer /></Provider>);

describe('<SearchBarContainer />', () => {
  describe('Instantiation', () => {
    it('should exist', () => {
      assert.isOk(SearchBarContainer);
    });
  });
});
