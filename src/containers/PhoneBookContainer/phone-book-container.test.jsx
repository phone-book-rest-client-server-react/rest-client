import React from 'react';
import { Provider } from 'react-redux';
import chai, { expect, assert } from 'chai';
import chaiEnzyme from 'chai-enzyme';
import { mount } from 'enzyme';
import PhoneBookContainer from './';
import { getAxios } from '../../services/actions';
import store from '../../services/store';

chai.use(chaiEnzyme());

const wrapper = mount(<Provider store={store}><PhoneBookContainer /></Provider>);

describe('<PhoneBookContainer />', () => {
  describe('Instantiation', () => {
    it('should exist', () => {
      assert.isOk(PhoneBookContainer);
    });
  });
});
