import React, { Component } from 'react';
import PhoneBook from '../../components/PhoneBook';
import { connect } from 'react-redux';
import { getAxios } from '../../services/actions';
import store from '../../services/store';

class PhoneBookContainer extends Component {
  constructor(props) {
    super(props);
    this.state = { phoneitems: [] };
    store.subscribe(() => {
      this.setState({
        phoneitems: store.getState().initPhoneBook.phonebook,
        nav: '',
        main: '',
      });
    });
  }
  
  componentWillUpdate(nextProps, nextState) {
    return (this.props.phoneitems !== nextProps);
  }
  
  componentDidMount() {
    getAxios();
  }
  
  render() {
    return <PhoneBook phoneitems={this.state.phoneitems} />;
  }
}

function mapStateToProps(state){
  return {
    phoneitems: state.phoneitems,
  }
}

export default connect(mapStateToProps)(PhoneBookContainer);
