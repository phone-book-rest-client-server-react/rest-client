import React from 'react';
import { Provider } from 'react-redux';
import chai, { expect, assert } from 'chai';
import chaiEnzyme from 'chai-enzyme';
import { mount } from 'enzyme';
import App from './App';
import store from './services/store';
import PhoneBookContainer from './containers/PhoneBookContainer';

chai.use(chaiEnzyme());

const wrapper = mount(<Provider store={store}><App /></Provider>);

describe('<App />', () => {
  describe('Instantiation', () => {
    it('should exist', () => {
      assert.isOk(App);
    });
  });
  describe('Layout', () => {
    it('has one div', () => {
      expect(wrapper.find('App')).to.have.length(1);
    });
  });
  describe('PhoneBookContainer inside', () => {
    it('should exist', () => {
      expect(wrapper.find('App').find('div').find('PhoneBookContainer')).to.have.length(1);
    });
  });
});
