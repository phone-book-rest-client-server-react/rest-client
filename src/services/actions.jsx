import axios from 'axios';
import store from './store';
export const GET_PHONE = 'GET_PHONE';
export const INIT_PHONE_BOOK = 'INIT_PHONE_BOOK';
export const TOGGLE_SCREEN = 'TOGGLE_SCREEN';

const API_URL = 'http://localhost:3001/phonebook'

export function toggleScreen(payload) {
  return { type: TOGGLE_SCREEN, payload };
}

export function getPhone(payload) {
  return { type: GET_PHONE, payload };
}

export function getPerson(payload) {
  return store.dispatch({
    type: GET_PHONE,
    payload
  });
}

export function toggleScreenFn(payload) {
  return store.dispatch({
    type: TOGGLE_SCREEN,
    payload: payload
  });
}

export function initPhoneBook(payload) {
  return { type: INIT_PHONE_BOOK, payload };
}

export function getAxios() {
  axios.get(API_URL)
  .then(response => {
  	return store.dispatch({
      type: INIT_PHONE_BOOK,
      payload: response.data
    });
  })
  .catch((error) => {
    console.log(error);
  })
}
