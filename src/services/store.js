import { createStore, applyMiddleware } from 'redux';
import axios from 'axios';
import axiosMiddleware from 'redux-axios-middleware';
import thunk from 'redux-thunk';
import reducer from './reducers';

function configureStore(initialState = { initPhoneBook: [], getPhone: null }) {
  const theRoot = axios.create({
    baseURL: 'http://localhost:3001/phonebook',
    responseType: 'json',
  });
  const theStore = createStore(
    reducer,
    initialState,
    applyMiddleware(thunk),
    axiosMiddleware(theRoot),
  );
  return theStore;
}

const store = configureStore();

export default store;
