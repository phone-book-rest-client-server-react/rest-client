import {combineReducers} from 'redux';
import { INIT_PHONE_BOOK, GET_PHONE, TOGGLE_SCREEN } from './actions';

export function initPhoneBook(state = { }, action) {
  switch (action.type) {
    case INIT_PHONE_BOOK:
      return Object.assign({}, state, {
        phonebook: action.payload,
      });
    default:
      return state;
  }
}

export function getPhone(state = { }, action) {
  switch (action.type) {
    case GET_PHONE:
      return Object.assign({}, state, {
        payload: action.payload,
      });
    default:
      return state;
  }
}

export function toggle(state = { }, action) {
  switch (action.type) {
    case TOGGLE_SCREEN:
      return Object.assign({}, state, {
        payload: action.payload,
      });
    default:
      return state;
  }
}

export default combineReducers({
	initPhoneBook,
	getPhone,
  toggle
});

