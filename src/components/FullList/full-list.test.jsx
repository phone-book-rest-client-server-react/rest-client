import React from 'react';
import chai, { expect, assert } from 'chai';
import chaiEnzyme from 'chai-enzyme';
import { shallow, mount } from 'enzyme';
import FullList from './';

chai.use(chaiEnzyme());

const props = {
  "phoneitems": [
    { "id": 10, "name": "Endel Jones", "phone": "+372.767.6543" },
    { "id": 11, "name": "Brian Bucket", "phone": "+44.9655543" },
    { "id": 12, "name": "Namaste Immer", "phone": "+36.904876543" }
  ]
};

const wrapper = mount(<FullList {...props} />);

describe('<FullList />', () => {
  describe('Instantiation', () => {
    it('should exist', () => {
      assert.isOk(FullList);
    });
  });
  describe('It should have a single main element', () => {
    it('should include one main element', () => {
      expect(wrapper.find('main')).to.have.length(1);
    });
  });
  describe('It should have a single ul element', () => {
    it('should include one ul element', () => {
      expect(wrapper.find('ul')).to.have.length(1);
    });
  });
  describe('It should have three li elements', () => {
    it('should include one ul element', () => {
      expect(wrapper.find('li')).to.have.length(3);
    });
  });
});

