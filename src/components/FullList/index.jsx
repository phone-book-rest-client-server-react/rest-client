import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PhoneItem from '../PhoneItem';

import './full-list.css';

class FullList extends Component {
  static propTypes = {
    phoneitems: PropTypes.array,
    toggleMe: PropTypes.func,
    className: PropTypes.string,
  }
  getPhoneItem ({name, phone, id}) {
    return <PhoneItem key={id} name={name} phone={phone} />;
  }
  render () {
    return (
      <main className={this.props.className}>
        <button className="leftside" onClick={this.props.toggleMe}>TOGGLE</button>
        <h2>all contacts</h2>
        <ul> {this.props.phoneitems.map(this.getPhoneItem)} </ul>
      </main>
    );
  }
}


export default FullList;

