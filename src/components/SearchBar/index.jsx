import React, { Component } from 'react';
import PropTypes from 'prop-types';
import store from '../../services/store';
import './search-bar.css';

class SearchBar extends Component {
  static propTypes={
    toggleMe: PropTypes.func,
    className: PropTypes.string,
  }
  constructor() {
    super();
    this.state={value:'',phoneitems:[], namelist:[]};
    store.subscribe((SearchBar) => {
      this.setState({
        phoneitems: store.getState().initPhoneBook.phonebook,
      });
    });
  }

  componentWillReceiveProps(nextProps) {
    return (this.props !== nextProps);
  }

  componentWillUpdate(nextProps, nextState) {
   return (this.props !== nextProps || this.state !== nextState); 
  }
  callPhone = (numToCall) => {
    const numberToCall=numToCall.currentTarget.innerHTML;
    window.alert(`About to call ${numberToCall}`);
  }
  getPhoneName = ({ id, name, phone }) => {
    return <li className="name-search" key={id}>{name} <br/><a onClick={this.callPhone}>{phone}</a></li>;
  }
  changed = (e) => {
    const threshold =1;
    let namelist = [];
    if(e.target.value.length > threshold){
      namelist = this.state.phoneitems.filter((phoneitem) => {
        return phoneitem.name.toLowerCase().indexOf(
          e.target.value.toLowerCase()) !== -1
      }).map(this.getPhoneName);
      }
    this.setState({value: e.target.value, namelist});
  }
  render() {
    return (
      <nav className={this.props.className}>
      <button className="leftside" onClick={this.props.toggleMe}>TOGGLE</button>
        <h2>Search Bar</h2>
        <div>
          <input
            onChange={this.changed} 
            type="text"
            value={this.state.value}
          />
        </div>
        <div>
          <ul className="list-wrapper">{this.state.namelist}</ul>
        </div>
      </nav>
    );
  }
}

export default SearchBar;

