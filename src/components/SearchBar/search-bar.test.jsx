import React from 'react';
import chai, { expect, assert } from 'chai';
import chaiEnzyme from 'chai-enzyme';
import { shallow } from 'enzyme';
import SearchBar from './';

chai.use(chaiEnzyme());

const wrapper = shallow(<SearchBar />);

describe('<SearchBar />', () => {
  describe('Instantiation', () => {
    it('should exist', () => {
      assert.isOk(SearchBar);
    });
  });
});
