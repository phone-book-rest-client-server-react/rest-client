import React, { Component } from 'react';
import PropTypes from 'prop-types';
import FullList from '../FullList';
import SearchBarContainer from '../../containers/SearchBarContainer';
import { toggleScreenFn } from '../../services/actions';
import './phone-book.css';

class PhoneBook extends Component {
  static propTypes = {
    toggleMe: PropTypes.func,
    phoneitems: PropTypes.array,
  }
  constructor(props) {
    super(props);
    this.state = {
      phoneitems: [],
      nav: '', main: '' };
  }
  toggleNav = () => {
    if (this.state.nav === 'full') {
      toggleScreenFn({nav: '', main: ''});
      this.setState({
        nav: '',
        main: '',
      });
    }else{
      toggleScreenFn({nav: 'full', main: 'empty'});
      this.setState({
        nav: 'full',
        main: 'empty',
      });
    }
  }
  toggleMain = () => {
    if (this.state.main === 'full') {
      toggleScreenFn({nav: '', main: ''});
      this.setState({
        nav: '',
        main: '',
      });
    }else{
      toggleScreenFn({nav: 'empty', main: 'full'});
      this.setState({
        nav: 'empty',
        main: 'full',
      });
    }
  }
  render () {
    return (
      <div className="phone-book">
        <h1>PhoneBook</h1>
        <SearchBarContainer
          className={this.state.nav}
          toggleMe={this.toggleNav} />
        <FullList
          className={this.state.main}
          toggleMe={this.toggleMain}
          phoneitems={this.props.phoneitems} />
     </div>
    );
  }
}

export default PhoneBook;

