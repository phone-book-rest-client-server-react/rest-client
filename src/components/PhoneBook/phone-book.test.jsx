import React from 'react';
import chai, { expect, assert } from 'chai';
import chaiEnzyme from 'chai-enzyme';
import { shallow } from 'enzyme';
import PhoneBook from './';

chai.use(chaiEnzyme());

const wrapper = shallow(<PhoneBook />);

describe('<PhoneBook />', () => {
  describe('Instantiation', () => {
    it('should exist', () => {
      assert.isOk(PhoneBook);
    });
  });
});
