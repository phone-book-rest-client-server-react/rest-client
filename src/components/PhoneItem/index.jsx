import React from 'react';

import './phone-item.css';

const PhoneItem = ({ name, phone }) => {
    return (
      <li className="phone-item">
        <p>{name} - {phone}</p>
      </li>
    );
}

export default PhoneItem;
