import React from 'react';
import chai, { expect, assert } from 'chai';
import chaiEnzyme from 'chai-enzyme';
import { shallow } from 'enzyme';
import PhoneItem from './';

chai.use(chaiEnzyme());

const data = {
  name: 'Phone Person',
  phone: '415.999.9999',
};

const wrapper = shallow(<PhoneItem {...data} />);

describe('<PhoneItem />', () => {
  describe('Instantiation', () => {
    it('should exist', () => {
      assert.isOk(PhoneItem);
    });
  });
  describe('It should have a single li element', () => {
    it('should include one li element', () => {
      expect(wrapper.find('li')).to.have.length(1);
    });
  });
});
